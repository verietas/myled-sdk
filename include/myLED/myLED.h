//
//  myLED.h
//  myLED
//
//  Created by Josh Pressnell on 1/7/13.
//  Copyright (c) 2013 myLED. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef enum
{
    myLED_Primary = 1,       // The primary LED.  myLED models may have different colors for primary LED
    myLED_Secondary = 2,     // The secondary LED.  myLED models may have different colors for secondary LED
} myLED_Type;

typedef enum
{
    myLED_FlashTypeSolo = 0,     // Single repeating flash
    myLED_FlashTypeDuo = 1,      // A double flash followed by a short pause, repeated
    myLED_FlashTypeConcerto = 2, // A double flash, a short pause, a single flash, a short pause, repeated
} myLED_FlashType;

// Used for debugging purposes
@protocol myLEDLoggingDelegate <NSObject>
- (void)logString:(NSString*)entry;
@end


@interface myLED : NSObject

+ (void)setDelegate:(id<myLEDLoggingDelegate>)delegate;

// Sets the enabled state for the SDK.  If the SDK is enabled, alerts fired via the below methods
// trigger an immediate signal to drive the myLED.  In the disabled state, the SDK treats
// fired events as if the myLED were not inserted and queues (delays) the myLED signal until
// the SDK is enabled again or the signal is canceled.
+ (void)enable:(Boolean)enable;
+ (Boolean)enabled;

// Sets the time stretch factor for the flash types.  Valid values are between 0.5 and 1.5.
// A value of 0.5 decreases the time delay between flashes by 50%, speeding up the flashes.
// A value of 1.5 increases the time delay between flashes by 50%, slowing the flashes down.
// The default value is 1.0.
+ (void)setTimeStretchFactor:(float)stretch;

// Flashes the indicated LED with the indicated flash type using the indicated priority.
// Priority is used as a way to optionally override (or not) existing alerts.  Any priority
// (1 being highest) will override any existing alerts with equal or lower priority.  Otherwise
// the alert is ignored.  If you don't wish to use priority, you can always specify 1.
//
// This version of the method flashes the sequence continually
//
+ (void)flashLED:(myLED_Type)led withType:(myLED_FlashType)flashType withPriority:(int)priority;

// Flashes the indicated LED with the indicated flash type using the indicated priority.
// Priority is used as a way to optionally override (or not) existing alerts.  Any priority
// (1 being highest) will override any existing alerts with equal or lower priority.  Otherwise
// the alert is ignored.  If you don't wish to use priority, you can always specify 1.
//
// This version of the method only flashes the sequence once and then stops.
//
+ (void)flashLEDOnce:(myLED_Type)led withType:(myLED_FlashType)flashType withPriority:(int)priority;

// Cancels flashing 
+ (void)cancel;

+ (float)primaryOffset;
+ (void)setPrimaryOffset:(float)offset;

@end
