//
//  AppDelegate.h
//  myLED Demo
//
//  Created by Josh Pressnell on 1/7/13.
//  Copyright (c) 2013 myLED. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
