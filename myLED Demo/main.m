//
//  main.m
//  myLED Demo
//
//  Created by Josh Pressnell on 1/7/13.
//  Copyright (c) 2013 myLED. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
