//
//  ViewController.m
//  myLED Demo
//
//  Created by Josh Pressnell on 1/7/13.
//  Copyright (c) 2013 myLED. All rights reserved.
//

#import "ViewController.h"
#import "myLED.h"

static Boolean primaryOn = NO;
static Boolean secondaryOn = NO;

@interface ViewController ()
@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
	// Do any additional setup after loading the view, typically from a nib.
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated
{
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)turnOnPrimary:(id)sender
{
    if( primaryOn )
        [myLED cancel];

    secondaryOn = NO;
    
    if( !primaryOn )
    {
        [myLED flashLED:myLED_Primary withType:primaryFlashType.selectedSegmentIndex withPriority:1];
    }
    primaryOn = !primaryOn;
}

- (void)turnOnSecondary:(id)sender
{
    if( secondaryOn )
        [myLED cancel];

    primaryOn = NO;
    
    if( !secondaryOn )
    {
        [myLED flashLED:myLED_Secondary withType:secondaryFlashType.selectedSegmentIndex withPriority:1];
    }
    secondaryOn = !secondaryOn;
}

- (void)flashTimingUpdate:(UISlider *)slider
{
    [myLED setTimeStretchFactor:slider.value];
}

@end
