//
//  ViewController.h
//  myLED Demo
//
//  Created by Josh Pressnell on 1/7/13.
//  Copyright (c) 2013 myLED. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController
{
    IBOutlet UISegmentedControl* primaryFlashType;
    IBOutlet UISegmentedControl* secondaryFlashType;
}

- (IBAction)turnOnPrimary:(id)sender;
- (IBAction)turnOnSecondary:(id)sender;
- (IBAction)flashTimingUpdate:(UISlider*)slider;

@end
